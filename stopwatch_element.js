/**
 * Javascript to make stopwatch element look like a progress bar
 */
(function ($) {
  /**
   * Make the progress bar count up or down every fraction of a second
   * @param dom_element stopwatch_element_bar the bar that will show progress
   * @param int max_val the maximum value of the progress bar.  In the stopwatch, it's the duration in milliseconds
   * @param boolean countdown true if this is a count down, else is a count up.
   */
  function progress( stopwatch_element_bar, max_val, countdown, elapsed_seconds ) {
    var timeout = 100;
    var increment = timeout;
    if( countdown ){
      increment *= -1;
    }
    var val = stopwatch_element_bar.progressbar( "value" ) || 0;
    stopwatch_element_bar.progressbar( "value", val + increment );
    var elapsed_seconds_value = parseFloat(elapsed_seconds.val());
    if( countdown ){
      elapsed_seconds_value -= increment * 0.001;
    } else {
      elapsed_seconds_value += increment * 0.001;
    }
    elapsed_seconds.val( elapsed_seconds_value );
    if ( !( (val+2*increment)>max_val || (val+2*increment)<0 ) ) {
      setTimeout( function() {
          progress( stopwatch_element_bar, max_val, countdown, elapsed_seconds );
        }, timeout );
    }
  }
  Drupal.behaviors.stopwatchElement = {
    attach: function( context, settings ) {
      $("div.stopwatch_element_bar:not(.stopwatch_element_processed)").each( function() {
        $(this).addClass("stopwatch_element_processed");
        var stopwatch_element_bar = $(this);
        var seconds = $(this).children("div.stopwatch_element_label").children("span.seconds");
        var max_value = parseFloat(seconds.html()) * 1000;
        var elapsed_seconds = $(this).children("input.stopwatch_element_elapsedseconds");
        elapsed_seconds.val(0);
        var countdown = $(this).hasClass("stopwatch_element_countdown");
        var val = 0;
        if( countdown ){
          val = max_value;
        }
        $(this).progressbar({
          max: max_value,
          value: val
        });
        // Wait one second and then decrement the bars.
        setTimeout( function() {
          progress( stopwatch_element_bar, max_value, countdown, elapsed_seconds );
        }, 1000 );
      });
    }
  }

})(jQuery);